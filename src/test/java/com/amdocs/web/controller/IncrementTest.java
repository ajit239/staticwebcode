package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
	@Test
	public void Testdecreasecounter() throws Exception {
		int k= new Increment().decreasecounter(0);
		assertEquals("Calculate",1,k);
		int l= new Increment().decreasecounter(1);
		assertEquals("Calculate",1,l);
		int m= new Increment().decreasecounter(2);
		assertEquals("Calculate",1,m);
		
	}
}